<?php

/**
 * Copyright (c) 2017, Thomas Schoffelen BV.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
//https://developer.apple.com/library/archive/documentation/UserExperience/Reference/PassKit_Bundle/Chapters/LowerLevel.html
//https://developer.apple.com/design/human-interface-guidelines/wallet/overview/pass-design/
//https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/PassKit_PG/Creating.html

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../src/PKPass.php');
use PKPass\PKPass;

// Replace the parameters below with the path to your .p12 certificate and the certificate password!
$pass = new PKPass('../Certificado_pass.p12', '5b94d0b380421e9edd645477cd114abd1988d346');

// Pass content
$data = [
    'description' => 'Reserva Movida',
    'formatVersion' => 1,
    'organizationName' => 'Movida',
    'passTypeIdentifier' => 'pass.net.megamil.parking', //https://developer.apple.com/account/ios/identifier/passTypeId
    'serialNumber' => '87654321',
    'teamIdentifier' => 'S8T5Q694LC', //https://developer.apple.com/account/#/membership/
    'eventTicket' => [ //Tipo de Ticket
        'headerFields' => [
            [
                'key' => 'title',
                'label' => 'Gerado: 13:59',
                'value' => '23/03/2019',
            ],
        ],
        'auxiliaryFields' => [
            [
                'key' => 'filial',
                'label' => 'Local devolução',
                'value' => 'MOVIDA X',
            ],
            [
                'key' => 'date',
                'label' => 'em',
                'value' => '30/04/2019 10:00',
            ],
            
        ],
        'secondaryFields' => [
            [
                'key' => 'filial',
                'label' => 'Local retirada',
                'value' => 'MOVIDA Y',
            ],
            [
                'key' => 'date',
                'label' => 'em',
                'value' => '30/03/2019 15:00',
            ],
            
        ],
        'backFields' => [
            [
                'key' => 'driver',
                'label' => 'Motorista',
                'value' => 'Eduardo dos santos',
            ],
        ],
        'transitType' => 'PKTransitTypeGeneric',
    ],
    'barcode' => [
        'format' => 'PKBarcodeFormatQR',
        'message' => 'TESTE QRCODE MOVIDA WALLET',
        'messageEncoding' => 'iso-8859-1',
        'altText' => 'QRCode',
    ],
    'backgroundColor' => 'rgb(242, 101, 34)',
    'labelColor' => 'rgb(255,255,255)',
    'foregroundColor' => 'rgb(255,255,255)',
    // 'logoText' => 'RESERVA',
    'relevantDate' => date('Y-m-d\TH:i:sP')
];
$pass->setData($data);

// Add files to the pass package
$pass->addFile('imagesMovida/icon.png');
$pass->addFile('imagesMovida/icon@2x.png');
$pass->addFile('imagesMovida/logo.png');
$pass->addFile('imagesMovida/strip.png');
$pass->addFile('imagesMovida/strip@2x.png');

// Create and output the pass
if(!$pass->create(true)) {
    echo 'Error: ' . $pass->getError();
}
