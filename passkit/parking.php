<?php

/**
 * Copyright (c) 2017, Thomas Schoffelen BV.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 */
//https://developer.apple.com/library/archive/documentation/UserExperience/Reference/PassKit_Bundle/Chapters/LowerLevel.html
//https://developer.apple.com/design/human-interface-guidelines/wallet/overview/pass-design/
//https://developer.apple.com/library/archive/documentation/UserExperience/Conceptual/PassKit_PG/Creating.html

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require('../src/PKPass.php');
use PKPass\PKPass;

// Replace the parameters below with the path to your .p12 certificate and the certificate password!
$pass = new PKPass('../Certificado_pass.p12', '5b94d0b380421e9edd645477cd114abd1988d346');

// Pass content
$data = [
    'description' => 'Estacionamento',
    'formatVersion' => 1,
    'organizationName' => 'Estacionamento',
    'passTypeIdentifier' => 'pass.net.megamil.parking', //https://developer.apple.com/account/ios/identifier/passTypeId
    'serialNumber' => '12345678',
    'teamIdentifier' => 'S8T5Q694LC', //https://developer.apple.com/account/#/membership/
    'eventTicket' => [ //Tipo de Ticket
        'headerFields' => [
            [
                'key' => 'title',
                'label' => 'Valor atual',
                'value' => 'R$ 15,00',
            ],
        ],
        'primaryFields' => [
            [
                'key' => 'parking',
                'label' => 'SÃO PAULO',
                'value' => 'Estacionamento X',
            ],
        ],
        'secondaryFields' => [
            [
                'key' => 'filial',
                'label' => 'Veiculo',
                'value' => 'GDZ-0746',
            ],
            [
                'key' => 'date',
                'label' => 'Entrou às:',
                'value' => '26/03/2019 10:00',
            ],
            
        ],
        'auxiliaryFields' => [
            [
                'key' => 'cost',
                'label' => 'até 22:00',
                'value' => 'R$ 15,00',
            ],
            [
                'key' => 'cost2',
                'label' => 'após 22:00 até 10:00',
                'value' => 'R$ 30,00',
            ],
            
        ],
        'backFields' => [
            [
                'key' => 'Motorista Nome',
                'label' => 'Nome',
                'value' => 'Eduardo dos santos',
            ],
        ],
        'transitType' => 'PKTransitTypeGeneric',
    ],
    // 'barcode' => [
    //     'format' => 'PKBarcodeFormatQR',
    //     'message' => 'TESTE QRCODE MOVIDA WALLET',
    //     'messageEncoding' => 'iso-8859-1',
    //     'altText' => 'Obrigado por usar nosso App',
    // ],
    'backgroundColor' => 'rgb(34,120,151)',
    'labelColor' => 'rgb(255,255,255)',
    'foregroundColor' => 'rgb(255,255,255)',
    // 'logoText' => 'RESERVA',
    'relevantDate' => '2019-03-23T00:21:26+00:00',//date('Y-m-d\TH:i:sP') W3C
    'expirationDate' => '2019-03-25T00:21:30+00:00'
];
$pass->setData($data);

// Add files to the pass package
$pass->addFile('images/icon.png');
$pass->addFile('images/icon@2x.png');
$pass->addFile('images/logo.png');
$pass->addFile('images/thumbnail.png');
$pass->addFile('images/strip.png');
$pass->addFile('images/strip@2x.png');

// Create and output the pass
if(!$pass->create(true)) {
    echo 'Error: ' . $pass->getError();
}
